#!/bin/sh

if [ "$(pgrep openvpn)" ]; then
    ip=$(dig +short myip.opendns.com @resolver1.opendns.com)
    country=$(curl -s ipinfo.io/$ip | grep "country" | awk '{gsub(/[",]/, ""); print $2}')
    echo " $country"
else
    echo ""
fi
