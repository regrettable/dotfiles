#!/usr/bin/env sh

IP=$(dig +short myip.opendns.com @resolver1.opendns.com)

if pgrep -x openvpn > /dev/null; then
    COUNTRY=$(curl ipinfo.io/$IP | grep "country" | awk '{gsub(/[",]/,""); print $2}')
	echo VPN: $IP $COUNTRY
else
	echo VPN: NA
fi
