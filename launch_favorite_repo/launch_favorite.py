#!/usr/bin/env python

import sys
import getopt
import subprocess
from shlex import quote
from os.path import expanduser


class Options:
    # This is what you want to change if you don't like the default values for arguments
    options_default = {
        "menu": "dmenu",
        # expanduser allows tilde to be used in the filename.
        "filename": expanduser("~/.config/.favorite_apps"),
        "delimiter": " = ",
        "shell": "/usr/bin/bash",
    }

    def __init__(self):
        self.options = self.options_default

    def set(option, value):
        self.options[option] = value

    def get_option_or_empty_string(self, option):
        return self.options.get(option, "")


class InputReader:
    def __init__(self, argv):
        self.options = Options()
        self.adjust_options_depending_on_inputs(argv)

    def get_options(self):
        return self.options

    def adjust_options_depending_on_inputs(self, argv):
        opts, _ = InputReader.get_args_from_user(argv)
        self.parse_user_inputs(opts)

    @staticmethod
    def get_args_from_user(args):
        try:
            return getopt.getopt(args, "hm:c:d:s:",
                                 ["shell=", "config=", "dmenu=", "delimiter="])
        except getopt.GetoptError as e:
            print(e, file=sys.stderr)
            InputReader.usage()
            sys.exit(1)

    def parse_user_inputs(self, opts):
        for option, value in opts:
            if option in ("-h", "--help"):
                FavoriteLauncher.usage()
                sys.exit(0)
            if option in ("-c", "--config"):
                self.options.set("filename", value)
            if option in ("-m", "--menu"):
                self.options.set("menu", value)
            if option in ("-d", "--delimiter"):
                self.options.set("delimiter", value)
            if option in ("-s", "--shell"):
                self.options.set("shell", value)

    @staticmethod
    def usage():
        print("""Usage: launch_favorite\n
                [-c | --config=] -- config_file, defaults to ~/.config/.favorite_apps\n
                [-m | --menu=] -- menu program to call, defaults to 'dmenu'\n
                [-d | --delimiter=] -- program name / executable delimiter
                [-h | --help] -- this menu\n
                [-s | --shell] -- shell executable to use\n""")


class FileParser:
    def __init__(self, filename, delimiter):
        self.apps = {}
        self.filename = filename
        self.delimiter = delimiter

        self.read_apps_from_file()

    def get_apps(self):
        return self.apps

    def open_filename(self):
        return safe_open(self.filename)

    def read_apps_from_file(self):
        apps_lines = self.open_filename()
        try:
            self.make_apps(apps_lines)
        except Exception as e:
            print(e, file=sys.stderr)
            sys.exit(1)
        finally:
            apps_lines.close()

    def make_apps(self, lines):
        lines_without_whitespace = [line.strip() for line in lines]

        for line in lines_without_whitespace:
            name, executable = tuple(line.split(self.delimiter))
            self.apps[name] = executable

    def get_apps_names(self):
        return quote("\n".join(self.apps.keys()))


class MenuMaker:
    def __init__(self, apps: str, menu):
        self.apps = apps
        self.menu = menu
        self.menu_command = self.make_menu_command()

    def run_menu_process(self):
        return subprocess.run(
            [self.menu_command], stdout=subprocess.PIPE, shell=True)

    @staticmethod
    def get_user_choice(process):
        return process.stdout.strip()

    def make_menu_command(self):
        return "echo " + self.apps + " | " + self.menu


class ProgramRunner:
    def __init__(self, menu_choice, apps, shell):
        self.menu_choice = menu_choice
        self.apps = apps
        self.shell = shell
        self.command_to_launch = self.get_command_to_launch()

    def launch(self):
        subprocess.run(
            [self.command_to_launch], shell=True, executable=self.shell)

    def get_command_to_launch(self):
        try:
            print(quote(self.apps[self.menu_choice]))
            return '`' + self.apps[self.menu_choice] + '`'
        except KeyError:
            # User pressed Esc
            sys.exit(0)


def safe_open(filename):
    try:
        return open(filename, "r")
    except OSError as e:
        print(e, file=sys.stderr)
        sys.exit(1)


def main(args):
    reader = InputReader(args)
    options = reader.get_options()

    filename = options.get_option_or_empty_string("filename")
    delimiter = options.get_option_or_empty_string("delimiter")
    menu = options.get_option_or_empty_string("menu")
    shell = options.get_option_or_empty_string("shell")

    parser = FileParser(filename, delimiter)
    apps = parser.get_apps()

    apps_str = parser.get_apps_names()
    menu_process = MenuMaker(apps_str, menu).run_menu_process()
    chosen_app = MenuMaker.get_user_choice(menu_process).decode("utf-8")
    runner = ProgramRunner(chosen_app, apps, shell)
    runner.launch()


if __name__ == "__main__":
    main(sys.argv[1:])
